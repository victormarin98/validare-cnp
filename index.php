<?php

function validareCNP ($inputString){
    if (strlen($inputString) != 13){
        return false; // Daca lungimea sirului este diferita de 13, inseamna ca nu avem un CNP valid.
    }

    if (!ctype_digit($inputString)){
        return false; // Daca avem orice caracter care nu este cifra, nu avem un CNP valid.
    }

    $digits = array();
    for ($i=0; $i < 13; $i++){
        $digits[$i] = intval($inputString[$i]); // Spargem string-ul intr-un array cu 13 cifre.
    }

    // In continuare, vom folosi variabile numite ca in textul cerintei pentru claritate (intr-un proiect real, nu denumim variabilele aa, ll, zz etc. :D )
    $s = $digits[0];
    $aa = $digits[1]*10 + $digits[2]; // anul nasterii
    $ll = $digits[3]*10 + $digits[4]; // luna nasterii
    $zz = $digits[5]*10 + $digits[6]; // ziua nasterii
    $jj = $digits[7]*10 + $digits[8]; // cod judet
    // intrucat nnn este random, nu avem ce sa validam pentru acest cod
    $c = $digits[12]; // cifra de control

    if ($s == 0){
        return false; // Prima cifra poate fi 1-9, asadar nu poate fi 0.
    }

    // aa poate avea orice valoare cuprinsa intre 0 si 99, asadar pentru moment nu avem ce valida.

    if ($ll > 12 || $ll == 0){
        return false; // Luna trebuie sa fie cuprinsa intre 1 si 12
    }

    // Vom stoca maximul de zile admis in functie de luna nasterii
    $maxDays = -1;
    switch ($ll){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            $maxDays = 31;
            break;
        case 2:
            if ($aa % 4 == 0){
                $maxDays = 29; // Anii bisecti sunt multipli de 4, asadar punem 29 de zile pentru februarie daca este an bisect.
            } else {
                $maxDays = 28;
            }
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            $maxDays = 30;
            break;
    }

    if ($zz == 0 || $zz > $maxDays){
        return false; // Daca zz nu se incadreaza in intervalul [1, maxDays], sirul nu este CNP valid.
    }

    if ($jj == 0 || $jj > 52){
        return false;  // Daca jj nu se incadreaza in intervalul [1, 52], sirul nu este CNP valid.
    }

    // Construim array-ul 279146358279 pentru a putea face parcurgerea intr-un loop.
    $controlArray = array(2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9);
    $sum = 0;
    for ($i = 0; $i < 12; $i++){
        $sum += ($digits[$i] * $controlArray[$i]);
    }

    $rezultatControl = ($sum % 11 == 10) ? 1 : ($sum % 11);
    if ($rezultatControl != $c){
        return false; // Daca rezultatul calculului nu corespune cu cifra de control, CNP-ul este invalid.
    }

    return true; // Daca sirul de caractere a trecut toate testele de mai sus, il putem considera un CNP valid.
}
